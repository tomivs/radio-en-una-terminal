/*
 * Script en C++ para escuchar y saber lo que suena por RadioÑú
 * Creado por: tomivs
 * Última edición: 19/11/2013 
 * Esto es posible gracias a la API: http://wiki.radiognu.org/api */

/*estas librerias parecen no ser necesarias
#include <ncurses.h>
#include <stdio.h>*/

#include <iostream>
#include <stdlib.h>
#include <fstream>

inline bool existe(const char * filename)
{
  return std::ifstream(filename);
}

using namespace std;
int main()
{
	if (existe("/usr/bin/mplayer") or existe("/bin/mplayer") or existe("/usr/bin/curl") or existe("/bin/curl") or existe("/usr/bin/python2") or existe("/bin/python2"))
	{
		if (existe("radiognu.py")){
			int seleccion;
			cout << ".::Script de RadioÑú::.\n\n";
			cout << "1. Escuchar la radio\n2. Ver lo que suena\n3. Escuchar y ver lo que suena\n\n";
			cout << "Seleccione una opción: ";
			cin>>seleccion;
			if (seleccion == 1)
			{
				system("mplayer http://audio.radiognu.org/radiometagnu.ogg");
				system("clear");
				main();
			}
			else if (seleccion == 2)
			{
				system("python2 radiognu.py");
				system("clear");
				main();
			}
			else if (seleccion == 3)
			{
				system("nohup mplayer http://audio.radiognu.org/radiognu.ogg &");
				system("clear");
				system("python2 radiognu.py");
				system("clear");
				main();
			}
			else { cout << "Opción inválida" << endl; }
		}//final del if de radiognu.py
		else {
				system("wget http://tomivs.com/experimentos/radiognu.py ");
				system("clear");
				main(); 
			}
	}else{cout << "Es necesario tener: curl, mplayer y python2\n";}
	return 0;
}
